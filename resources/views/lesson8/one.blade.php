@extends('layouts.app')

@section('aside')
    @parent
    <p>{{ $aside }}</p>
@endsection