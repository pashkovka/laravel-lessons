@if(Session::has('success'))
    {{ Session::get('success') }}
@endif
<table>
    <tr>
        <th>ID</th>
        <th>TITLE</th>
        <th>DESC</th>
        <th>DATE</th>
        <th>EDIT</th>
        <th>DELETE</th>
    </tr>
@foreach($posts as $post)
<tr>
    <td>{{ $post->id }}</td>
    <td><a href="{{ route('post', $post->id) }}">{{ $post->title }}</a></td>
    <td>{{ $post->short_desc }}</td>
    <td>{{ $post->create_at }}</td>
    <td><a href="{{ route('post.edit', $post->id) }}">Редактировать</a></td>
    <td><a href="{{ route('post.del', $post->id) }}">Удалить</a></td>
</tr>
@endforeach
</table>