@if(Session::has('success'))
    {{ Session::get('success') }}
@endif
<form action="" method="POST">
    @csrf
    <input name="title" value="{{ $post->title }}" style="width: 300px">
    <p><input name="short_desc" value="{{ $post->short_desc }}"></p>
    <input name="create_at" value="{{ $post->create_at }}">
    <p><textarea name="post">{{ $post->post }}</textarea></p>
    <input name="submit" type="submit">
</form>

<p><a href="{{ route('post.all') }}">Все посты</a></p>