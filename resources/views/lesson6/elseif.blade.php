<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

@if($index == 1 or $index == 2 or $index == 12)
    <p>Зима</p>
    @elseif($index > 2 and $index < 6)
    <p>Весна</p>
    @elseif($index > 5 and $index < 9)
    <p>Лето</p>
    @elseif($index > 8 and $index < 12)
    <p>Осень</p>
    @else
    <p>Не известный номер месяца</p>
@endif


</body>
</html>