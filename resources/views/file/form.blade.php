<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File upload</title>
</head>
<body>

<form action="/file/upload" method="post" enctype="multipart/form-data">
    @csrf
    <input type="file" name="new_file">
    <input type="submit">
</form>

@isset($path)
<p><img src="{{ asset('storage/'.$path) }}" alt="kuyfu"></p>
@endisset

</body>
</html>