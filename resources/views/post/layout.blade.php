<!DOCTYPE html>
<html>
<head>
    <title>{{ $title }}</title>
</head>
<body>
<header>
    <h1>{{ $title }}</h1>
</header>
<main>
    @section('content')
    @show
</main>
</body>
</html>