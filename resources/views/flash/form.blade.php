
<p>{{ $sum ?? 'Not number' }}</p>

<form action="{{ route('flash.form.result') }}" method="post">
    @csrf
    <input type="number" name="one" value="{{ old('one') }}">
    <input type="number" name="two" value="{{ old('two') }}">
    <input type="number" name="three" value="{{ old('three') }}">
    <input type="number" name="four" value="{{ old('four') }}">
    <input type="number" name="five" value="{{ old('five') }}">
    <input type="submit">
</form>