@extends('products.layout')
@section('content')
    <div class="product">
        <h4><a href="/product/{{ $cat_id }}">{{ $category }}</a></h4>
        <h2>{{ $product['name'] }}</h2>
        <span>Цена: {{ $product['cost'] }}</span>
        <span>Наличие:
        @if($product['inStock'])
                В наличии
            @else
                Нет в наличии
            @endif
        </span>
        <div class="text">
            {{ $product['desc'] }}
        </div>
    </div>
@endsection
