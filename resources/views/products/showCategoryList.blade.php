@extends('products.layout')
@section('content')
    @foreach($categories as $category)
        <h3><a href="/product/{{ $category['id'] }}">{{ $category['name'] }}</a></h3>
        <p>Постов {{ $category['count'] }} шт.</p>
        <hr>
    @endforeach
@endsection