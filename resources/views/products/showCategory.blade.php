@extends('products.layout')
@section('content')
    <h3>{{ $category }}</h3>
    @foreach($products as $key => $product)
    <div class="product">
        <h4><a href="/product/{{ $id }}/{{ $key }}">{{ $product['name'] }}</a></h4>
        <p>{{ $product['cost'] }}</p>
        <hr>
    </div>
    @endforeach
@endsection