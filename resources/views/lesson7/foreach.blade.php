<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<ul>
    @foreach($nums as $key => $num)
        <li>{{ $key + 1 }} - {{ $num }}</li>
    @endforeach
</ul>

<ul>
    @foreach($nums as $key => $num)
        <li>{{ $key + 1 }} - {{ $num ** 2 }}</li>
    @endforeach
</ul>

<ul>
    @foreach($nums as $key => $num)
        <li>{{ $key + 1 }} - {{ sqrt($num) }}</li>
    @endforeach
</ul>

<p>------------------------------------------------------------------------------</p>


<ul>
    @foreach($nums as $key => $num)
        @if($num % 2 == 0)
            <li>{{ $key + 1 }} - {{ $num }}</li>
        @endif
    @endforeach
</ul>

@if(is_array($date))
    <ul>
        @foreach($date as $d)
            <li>{{ $d }}</li>
        @endforeach
    </ul>
@else
    {{ $date }}
@endif


<table style="border: solid 1px #1b1e21">

    @foreach($table as $tr)
        <tr>
            @foreach($tr as $td)
                <td>{{ $td }}</td>
            @endforeach
        </tr>
    @endforeach
</table>

<ul>
    @foreach ($employees as $employee)
        <li>{{ $employee['name'] }} {{ $employee['surname'] }} {{ $employee['salary'] }}</li>
    @endforeach
</ul>

<table style="border: solid 1px #1b1e21">
    @foreach ($employees as $employee)
        <tr style="border: solid 1px #1b1e21">
            @foreach($employee as $key => $item)
                <td style="border: solid 1px #1b1e21">{{ $key }}: {!! $item !!}</td>
            @endforeach
        </tr>
    @endforeach
</table>

<ul>
    @foreach($strings as $string)
        @if($loop->first)
            @php
                $class = ' class="first"';
            @endphp
        @elseif($loop->last)
            @php
                $class = ' class="last"';
            @endphp
        @else
            @php
                $class = '';
            @endphp
        @endif
        @if($loop->remaining > 3)
            <li{!! $class !!}>{{ $loop->iteration }} - {{ $loop->index }} - <b>{{ $string }}</b></li>
        @else
            <li{!! $class !!}>{{ $loop->iteration }} - {{ $loop->index }} - <i>{{ $string }}</i></li>
        @endif
    @endforeach
</ul>

@for($i=1; $i<=10; $i++)
    <p>
        @foreach(range(1,10) as $j)
            {{ $j }},
        @endforeach
    <p>

@endfor

</body>
</html>