<!DOCTYPE html>
<html>
<head>
    <title>My view</title>
</head>
<style>
    .red{
        color: red;
    }
</style>
<body>

<p class="{{ $first_class }}">Имя: {{ $name }}</p>
<p{!! $style !!}>Фамилия: {{ $surname }}</p>
<p>Зарплата: {{ $salary }}</p>
<p><a href="{{ $href }}">{{ $text }}</a></p>

<form>
    <input type="text" value="{{ $value1 }}"><br>
    <input type="text" value="{{ $value2 }}"><br>
    <input type="text" value="{{ $value3 }}"><br>
</form>

<p>Дата: {{ date('d.m.Y') }}</p>

<p>{{ $item['name'] ?? 'I don\'t have name' }}</p>
<p>{{ $item['surname'] ?? 'I don\'t have surname' }}</p>
<p>{{ $item['salary'] ?? 'I don\'t have salary' }}</p>
<p>Всего пользователей: {{ count($employees) }}</p>
<p>{{ $city ?? 'Москва' }}</p>

<h3>Location:</h3>
@foreach($location as $item)
    <p>
        {!! $item['country'] ?? '<b>Россия</b>' !!} - {!! $item['city'] ?? '<b>Москва</b>' !!}
    </p>
@endforeach

<h3>Date:</h3>
<p>
    Год: {{ $year ?? date('Y') }}<br>
    Месяц: {{ $month ?? date('m') }}<br>
    День: {{ $day ?? date('d') }}<br>
</p>
<p>{!! $str !!}</p>

{{--Это комментарий--}}

<p>
    @php
        echo 'This is PHP';
    @endphp
</p>
</body>
</html>