@extends('layouts.app')

@section('content')
    <table>
        <tr>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Статус</th>
        </tr>
        @foreach($users as $user)
            @if($user['banned'])
                @php($active = 'забанен')
                @php($style = 'color: red')
            @else
                @php($active = 'активен')
                @php($style = 'color: green')
            @endif
            <tr style="{{ $style }}">
                <td>{{ $user['name'] }}</td>
                <td>{{ $user['surname'] }}</td>
                <td>{{ $active }}</td>
            </tr>
        @endforeach
    </table>

    <form action="#">
        @foreach($strings as $string)
            <br><input type="text" value="{{ $string }}"><br>
        @endforeach
    </form>

    <p>
        <select name="#" id="5">
            @foreach($strings as $string)
                <option value="{{ $string }}">{{ $string }}</option>
            @endforeach
        </select>
    </p>

    <ul>
        @foreach($days as $day)
            @if($day == $current_day)
                @php($style = ' class="active"')
            @else
                @php($style = '')
            @endif
            <li{!! $style !!}>{{ $day }}</li>
        @endforeach
    </ul>
@endsection