@extends('layouts.app')

@section('content')
    <table>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Зарплата</th>
        </tr>
        @foreach($employees as $employee)
            @if($employee['salary'] > 2000)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $employee['name'] }}</td>
                    <td>{{ $employee['surname'] }}</td>
                    <td>{{ $employee['salary'] }}</td>
                </tr>
            @endif
        @endforeach
    </table>
@endsection