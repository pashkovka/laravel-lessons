@extends('layouts.app')

@section('content')
    <ul>
        @foreach($links as $link)
            <li><a href="http://{{ $link['href'] }}">{{ $link['text'] }}</a></li>
        @endforeach
    </ul>
@endsection