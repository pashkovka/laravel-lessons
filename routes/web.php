<?php

Route::get('/', function () {
    return view('welcome');
});


// Контроллеры
Route::get('test/show', 'TestController@show');
Route::get('/pages/show/{id}', 'Page@showOne')->where(['id'=>'[0-9]+']);
Route::get('/test/sum/{num1}/{num2}', 'Test@sum')->where(['num1'=>'[0-9]+', 'num2'=>'[0-9]+']);

Route::get('/employee/show/{id}', 'EmployeeController@showOne')->where(['id'=>'[0-9]+']);
Route::get('/employee/show/fild/{id}-{fild}', 'EmployeeController@showField')->where(['id'=>'[0-9]+', 'fild'=>'[a-z]+']);
Route::get('/lesson6/{param}', 'Lesson6Controller@if')->where(['param'=>'[1-7]{1}']);
Route::get('/lesson6/elseif/{param}', 'Lesson6Controller@elseif')->where(['param'=>'[0-9]{1,2}']);
Route::get('/lesson6/unless/{param}', 'Lesson6Controller@unless')->where(['param'=>'[0-9]{1,3}']);
Route::get('/lesson6/sum', 'Lesson6Controller@sum');
Route::get('/lesson7/foreach', 'Lesson7Controller@foreach');

Route::get('/lesson8/one', 'Lesson8Controller@one');

Route::get('/lesson9/one', 'Lesson9Controller@one');
Route::get('/lesson9/three', 'Lesson9Controller@three');
Route::get('/lesson9/seven', 'Lesson9Controller@seven');

Route::get('/posts/{id}', 'PostController@showOne');
Route::get('/posts', 'PostController@showAll');

Route::get('/product/{category_id}/{product_id}', 'ProductController@showProduct');
Route::get('/product/{category_id}', 'ProductController@showCategory');
Route::get('/categories', 'ProductController@showCategoryList');

Route::get('/form1', 'Form1Controller@one');
Route::post('/form1/result', 'Form1Controller@result');
Route::match(['post','get'],'/form1/match', 'Form1Controller@match');

Route::get('/form3/form', 'Form3Controller@form');
Route::post('/form3/result', 'Form3Controller@result');

Route::get('/test/method', 'MethodAndPathController@test');

Route::get('session/one', 'SessionController@one');
Route::post('session/two', 'SessionController@two');

Route::match(['post','get'],'redirect/one', 'RedirectController@one');
Route::get('redirect/two/{param1?}/{param2?}', 'RedirectController@two')->name('redirect.two');

Route::get('flash/one', 'FlashController@one');
Route::get('flash/two', 'FlashController@two')->name('flash.two');
Route::get('flash/form', 'FlashController@form');
Route::post('flash/form/result', 'FlashController@result')->name('flash.form.result');

Route::get('responce', 'ResponseController@show');
Route::get('responce2', 'ResponseController@show2');

Route::get('cookie/input', 'CookieController@userInput');
Route::get('cookie/next', 'CookieController@nextInput')->name('cookie.next');

Route::get('file/form', 'FileController@form');
Route::post('file/upload', 'FileController@upload');

Route::get('query/builder/one', 'QueryBuilder1Controller@one');
Route::get('query/builder/employees', 'EmployeeDbController@all');

Route::get('query/builder/insert', 'DbController@insert');
Route::get('query/builder/update', 'DbController@update');
Route::get('query/builder/delete', 'DbController@delete');
Route::get('query/builder/join', 'DbController@join');

Route::get('post/all/{order?}/{dir?}', 'PostController2@getAll')
    ->where([
        'order'=>'id|title|date',
        'dir'=>'asc|desc'
    ])->name('post.all');

Route::get('post/{id}', 'PostController2@getOne')->where(['id'=>'[0-9]+'])->name('post');
Route::get('post/new', 'PostController2@newPost');
Route::post('post/new', 'PostController2@addPost');
Route::get('post/update', 'PostController2@update');
Route::match(['post','get'],'post/edit/{id}', 'PostController2@editPost')->name('post.edit');
Route::get('post/del/{id}', 'PostController2@delPost')->name('post.del');


Route::get('user/profile/{id}', 'RelationshipController@user');
Route::get('users/profile', 'RelationshipController@profile');



























