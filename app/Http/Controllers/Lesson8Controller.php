<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Lesson8Controller extends Controller
{
    public function one()
    {
        $title = 'ONE';
        $aside = 'Content ONE for aside';
        return view('lesson8.one', compact('title', 'aside'));
    }
}
