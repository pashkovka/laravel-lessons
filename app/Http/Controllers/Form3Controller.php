<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Form3Controller extends Controller
{
    public function form() {
        return view('form3.form');
    }


    public function result(Request $request) {
//        dump($request->all());
        return view('form3.all',[
//          'data' => $request->all(),
//          'data' => $request->except(['text2']),
          'data' => $request->only(['text']),
        ]);
    }
}
