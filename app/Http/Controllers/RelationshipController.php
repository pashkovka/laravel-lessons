<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RelationshipController extends Controller
{
    public function user($id)
    {
        $user = User::with('profile')->findOrFail($id);

        return view('relationships.user', compact('user'));
    }

    public function profile()
    {
        $users = User::with('profile')->get();

        return view('relationships.users', compact('users'));
    }
}
