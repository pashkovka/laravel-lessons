<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FlashController extends Controller
{
    public function one(Request $request)
    {
        $request->session()->flash('key', 'Это флеш сообщение');
        return redirect()->route('flash.two');
    }

    public function two(Request $request)
    {
        return $request->session()->get('key');
    }

    public function form()
    {
        return view('flash.form');
    }

    public function result(Request $request)
    {
        $request->flash();

        $sum = $request->old('one')
        + $request->old('two')
        + $request->old('three')
        + $request->old('four')
        + $request->old('five');

        return view('flash.form', compact('sum'));
    }
}
