<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueryBuilder1Controller extends Controller
{
    public function one()
    {
        $users = DB::select("select * from users where age > :age",[':age'=>18]);

        DB::delete("delete from users where id > :id", [5]);

        DB::insert("insert into users (name, surname, age) values (?,?,?)", ['Ivan','Ivanov',73]);

        DB::update('update users set name = ? where name = ?', ['John','Ivan']);


        return view('query_builder.one', compact('users'));
    }
}
