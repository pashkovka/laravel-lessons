<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function one(Request $request) {
        $num = (int)$request->input('num');

        if ($num > 0 and $num < 11) {
//            return redirect('redirect/two')->withInput();
            return redirect()->route('redirect.two',[
              'param1'=>1,
              'param2'=>2,
            ])->withInput();
        }else{
            $num = NULL;
        }

        return view('redirect.one', ['num'=>$num]);
    }


    public function two(Request $request, $param1 = 0, $param2 = 0){
        dump($request->old('num'), $param1, $param2);
        return '<b>Форма отправлена!</b>';
    }


    public function three(Request $request) {

    }
}
