<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CookieController extends Controller
{
    public function userInput(Request $request)
    {
        return response("<p>Hello world! <a href=\"/cookie/next\">Далее</a></p>")
          ->withCookie(
            cookie()->forever('time_input', time())
          );
    }


    public function nextInput(Request $request)
    {
//        dd($request->cookie('time_input'));
        return response()->view('cookie.test', [
          'time_input'=>$request->cookie('time_input')
        ]);
    }
}
