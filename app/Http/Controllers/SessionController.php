<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller {
    public function one(Request $request) {
        $request->session()->put('user_data', 'user_value');
        if (!$request->session()->has('cnt')) {
            $request->session()->put('cnt', 0);
        }
        $request->session()->put('cnt', $request->session()->get('cnt') + 1);
        $cnt = $request->session()->get('cnt');

///////////////////////////////////////////////////////////////////////////////
        $value = $request->session()->get('counter', 1);
        // Увеличим счетчик на 1:
        $counter = $request->session()->put('counter', $value + 1);
        $counter = $counter ?? $value;
        //////////////////////////////////////

        $request->session()->put('time', time());
        $time = $request->session()->get('time');
        ////////////////////////////////////////////////

        $request->session()->put('arr', [1, 2, 3, 4]);
        $request->session()->push('arr', 77);
        $arr = $request->session()->get('arr');
        ////////////////////////////////////////

        $request->session()->put('key', 'value');
        $request->session()->forget('key');
        // Метод pull прочитывает и удаляет элемент из сессии за одно действие:
        $value = $request->session()->pull('key', 'default');


        //$request->session()->flush();


        // Сохранить данные в сессию:
        session(['key' => 'value']);

        // Получить данные из сессии:
        $value = session('key');

        // При получении указать значение по умолчанию:
        $value = session('key', 'default');

        return view('session.one', compact('cnt', 'counter', 'time', 'arr'));
    }


    public function two(Request $request) {
        return view('session.two', [
          'ses' => $request->session()
            ->get('user_data')
        ]);
    }
}
