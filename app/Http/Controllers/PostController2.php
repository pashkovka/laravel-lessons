<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class PostController2 extends Controller
{
    use SoftDeletes;


    public function getAll($sort = 'date', $dir = 'desc')
    {
        if ($dir == 'desc'){
            $method = 'sortByDesc';
        }else{
            $method = 'sortBy';
        }
        $posts = Post::all()->$method($sort);
//            ->sortBy('date');

        return view('posts2.all', compact('posts'));
    }


    public function getOne($id)
    {
        $post = Post::findOrFail($id);

        return view('posts2.one', compact('post'));
    }


    public function newPost()
    {
        return view('posts2.new');
    }


    public function addPost(Request $request)
    {
        Post::create($request->all());
        return back();
    }


    public function update()
    {
        $post = Post::find(1);
        $post->title = 'Новый заголовок записи с id 1';

        $post->save();
    }


    public function editPost(Request $request, $id)
    {
        $post = Post::findOrFail($id);
//        $post->update($request->all());

        if ($request->has('submit')) {
            $post->title = $request->title;
            $post->short_desc  = $request->short_desc;
            $post->create_at  = $request->create_at;
            $post->post  = $request->post;

            $post->save();

            $request->session()->flash('success', 'Post save!');
        }

        return view('posts2.edit', compact('post'));
    }


    public function delPost(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        $request->session()->flash('success', 'Post "'.$post->title.'" deleted!');

        return redirect()->route('post.all');
    }
}
