<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    private $employees;

    public function __construct()
    {
        $this->employees = [
            1 => [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            2 => [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            3 => [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
            4 => [
                'name' => 'user4',
                'surname' => 'surname4',
                'salary' => 4000,
            ],
            5 => [
                'name' => 'user5',
                'surname' => 'surname5',
                'salary' => 5000,
            ],
        ];
    }


    public function showOne($id)
    {
        $employee = $this->employees[$id] ?? 'User not find!';

            //return 'Name: ' . $employee['name'] . ' | Surname: ' . $employee['surname'] . ' | Salary: ' . $employee['salary'];
        $first_class = 'red';

        return view('employee.showOne',
            [
                'name'=>$employee['name'] ?? 'Don\'t have name',
                'surname'=>$employee['surname'] ?? 'Don\'t have surname',
                'salary' => $employee['salary'] ?? 'Don\'t have salary',
                'first_class'=>$first_class,
                'style' => ' style="color: red"',
                'value1'=>'value1',
                'value2'=>'value2',
                'value3'=>'value3',
                'text'=>'текст ссылки',
                'href'=>'https://google.com',
                'item'=>$employee,
                'employees'=>$this->employees,
                'city'=>null,
                'location'=> [
                    ['country'=>'Абхазия', 'city'=>'Сухум'],
                    ['country'=>'Австралия', 'city'=>'Канберра'],
                    ['country'=>'Австрия', 'city'=>'Вена'],
                    ['country'=>null, 'city'=>'Баку'],
                    ['country'=>'Албания', 'city'=>'Тирана'],
                    ['country'=>'Алжир', 'city'=>null],
                    ['country'=>'Ангола', 'city'=>'Луанда'],
                    ['country'=>null, 'city'=>null],
                ],
                'year'=>null,
                'month'=>null,
                'day'=>null,
                'str'=>'<b>строка</b>',
            ]
        );
    }


    public function showField($id, $fild)
    {
        $employee = $this->employees[$id] ?? 'User not find!';
        if (is_array($employee))
            return $employee[$fild] ?? "User " . $id . " don't have fild " . $fild;
    }
}
