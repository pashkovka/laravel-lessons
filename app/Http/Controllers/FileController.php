<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function upload(Request $request)
    {
        $this->validate($request, [
            'new_file' => 'required|mimes:jpg,jpeg'
        ]);

        //-----------------------------------------------// имя папки       имя диска  (config/filesystems.php)
        $path = $request->file('new_file')->store('new_dir', 'public');

        return view('file.form', ['path' => $path]);
    }


    public function form()
    {
        return view('file.form');
    }
}
