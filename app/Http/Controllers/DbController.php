<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


// SELECT -> app/Http/Controllers/EmployeeDbController.php

class DbController extends Controller
{
    public function insert()
    {
        DB::table('users')->insert([
            [
                'login' => 'hac2',
                'password' => bcrypt('123'),
                'email' => 'email2@email.com'
            ],
            [
                'login' => 'hac3',
                'password' => bcrypt('123'),
                'email' => 'email3@email.com'
            ],
            [
                'login' => 'hac4',
                'password' => bcrypt('123'),
                'email' => 'email4@email.com'
            ],
        ]);
    }


    public function update()
    {
        DB::table('users')->where('email', 'email3@email.com')
            ->update([
            'email'=>'update_email@mail.com'
        ]);
    }


    public function delete()
    {
        DB::table('users')
            ->where('email', 'email2@email.com')
            ->delete();
    }


    public function join()
    {
        $products = DB::table('products')
            ->leftJoin('categories', 'products.id_category', '=', 'categories.id')
            ->get();

        var_dump($products);
    }
}
