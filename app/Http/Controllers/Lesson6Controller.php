<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Lesson6Controller extends Controller
{
    public function if($param)
    {
        return view('lesson6.if', ['index'=>$param]);
    }


    public function elseif($param)
    {
        return view('lesson6.elseif', ['index'=>$param]);
    }

    public function unless($param)
    {
        if ($param <= 17)
            $age = false;
        else
            $age = true;
        return view('lesson6.unless', compact('age'));
    }

    public function sum()
    {
        $nums = [1,2,3,4,5,6,7];
//        $nums = [];
        return view('lesson6.sum', compact('nums'));
    }
}
