<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeDbController extends Controller
{
    public function all()
    {
//        $employees = DB::table('employees')->select('name', 'salary')->get();
//        $employees = DB::table('employees')->where('salary',500)->get();
//        $employees = DB::table('employees')->where('salary','>', 450)->get();
//        $employees = DB::table('employees')->where('salary','<>', 500)->get();
//        $employees = DB::table('employees')->where('salary', 400)->orWhere('id', '>', 4)->get();
//        $employees = DB::table('employees')->pluck('name');
//        $employees = DB::table('employees')->whereBetween('salary', [450,1100])->get();
//        $employees = DB::table('employees')->whereNotBetween('salary', [300,600])->get();
//        $employees = DB::table('employees')->whereIn('id',[1,2,3,5])->get();
//        $employees = DB::table('employees')->whereNotIn('id',[1,2,3,5])->get();
//        $employees = DB::table('employees')->whereSalaryOrPosition(500,'программист')->get();


//        $events = DB::table('events')->whereColumn('start', 'finish')->get();

//        var_dump($events);

//        $employees = DB::table('employees')->orderBy('salary')->get();
//        $employees = DB::table('employees')->orderBy('birthday', 'desc')->get();
//        $employees = DB::table('employees')->max('salary');
//        $employees = DB::table('employees')->sum('salary');
//        $employees = DB::table('employees')->avg('salary');


//        $employees = DB::select("select position, min(salary) as aggregate from employees group by position");
//        $employees = DB::select("select position, sum(salary) as aggregate from employees group by position");

//        $employees = DB::table('employees')->whereDate('birthday','1988-03-25')->get();
//        $employees = DB::table('employees')->whereDay('birthday','25')->get();
//        $employees = DB::table('employees')->whereMonth('birthday','03')->get();
        $employees = DB::table('employees')->whereYear('birthday','1990')->get();

        var_dump($employees);

        return '';
    }
}
