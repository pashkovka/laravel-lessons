<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MethodAndPathController extends Controller
{
    public function test(Request $request) {
        var_dump($request->path());
        var_dump($request->url());
        var_dump($request->fullUrl());
        $url = $request->fullUrlWithQuery(['p2'=>2,'p3'=>3]);
        var_dump($url);

        if ($request->is('admin/*')) {
            //
        }
    }
}
