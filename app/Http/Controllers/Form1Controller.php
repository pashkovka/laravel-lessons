<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Form1Controller extends Controller {
    public function one() {

        return view('form1.one');
    }


    public function result(Request $request) {

        return view('form1.result', [
            'number' => $request->text,
            'number2' => $request->text2,
            'number3' => $request->text3,
            'method' => $request->method()
          ]
        );
    }


    public function match(Request $request) {


        return view('form1.match', [
          'number' => $request->text,
          'number2' => $request->text2,
          'method' => $request->method()
        ]);
    }
}
