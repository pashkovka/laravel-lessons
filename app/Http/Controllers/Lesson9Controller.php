<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Lesson9Controller extends Controller
{
    public $title = 'Lesson 9';

    public function one()
    {
        $links = [
            [
                'text' => 'text1',
                'href' => 'href1',
            ],
            [
                'text' => 'text2',
                'href' => 'href2',
            ],
            [
                'text' => 'text3',
                'href' => 'href3',
            ],
        ];

        return view('lesson9.one', ['links'=>$links, 'title'=>$this->title . ' - one']);
    }


    public function three()
    {
        $employees = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
            [
                'name' => 'user4',
                'surname' => 'surname4',
                'salary' => 4000,
            ],
            [
                'name' => 'user5',
                'surname' => 'surname5',
                'salary' => 5000,
            ],
        ];

        return view('lesson9.three', ['employees'=>$employees, 'title'=>$this->title . ' - three']);
    }


    public function seven()
    {
        $users = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'banned' => true,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'banned' => false,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'banned' => true,
            ],
            [
                'name' => 'user4',
                'surname' => 'surname4',
                'banned' => false,
            ],
            [
                'name' => 'user5',
                'surname' => 'surname5',
                'banned' => false,
            ],
        ];

        $strings = [
            'string1',
            'string2',
            'string3',
            'string4',
            'string5',
            'string6',
        ];

        $days = range(1, (int) date('t'));
        $current_day = (int) date('d');

        return view('lesson9.seven', [
            'users'=>$users,
            'title'=>$this->title . ' - seven',
            'strings' => $strings,
            'days'=>$days,
            'current_day'=>$current_day
        ]);
    }
}
