<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Lesson7Controller extends Controller
{
    public function foreach()
    {
        $nums = range(0, 10);
        $date = range(1, 6);
//        $date = 10;
        $table = [range(1,5),range(1,5),range(1,5),range(1,5),range(1,5)];
        $employees = [
            [
                'name' => 'user1',
                'surname' => 'surname1',
                'salary' => 1000,
            ],
            [
                'name' => 'user2',
                'surname' => 'surname2',
                'salary' => 2000,
            ],
            [
                'name' => 'user3',
                'surname' => 'surname3',
                'salary' => 3000,
            ],
        ];
        $strings = [
            'string1',
            'string2',
            'string3',
            'string4',
            'string5',
            'string6',
            'string7',
            'string8',
        ];
        return view('lesson7.foreach', compact('nums', 'date', 'table', 'employees', 'strings'));
    }
}
