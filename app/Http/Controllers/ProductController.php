<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $categories;

    public function __construct()
    {
        $this->categories = [
            1 => [
                'name' => 'Категория 1',
                'products' => [
                    1 => [
                        'name' => 'Продукт 1',
                        'cost' => '300',
                        'inStock' => true,
                        'desc' => 'Описание продукта 1'
                    ],
                    2 => [
                        'name' => 'Продукт 2',
                        'cost' => '400',
                        'inStock' => true,
                        'desc' => 'Описание продукта 2'
                    ],
                    3 => [
                        'name' => 'Продукт 3',
                        'cost' => '500',
                        'inStock' => false,
                        'desc' => 'Описание продукта 3'
                    ],
                ],
            ],
            2 => [
                'name' => 'Категория 2',
                'products' => [
                    1 => [
                        'name' => 'Продукт 1',
                        'cost' => '700',
                        'inStock' => true,
                        'desc' => 'Описание продукта 1'
                    ],
                    2 => [
                        'name' => 'Продукт 2',
                        'cost' => '800',
                        'inStock' => false,
                        'desc' => 'Описание продукта 2'
                    ],
                    3 => [
                        'name' => 'Продукт 3',
                        'cost' => '900',
                        'inStock' => false,
                        'desc' => 'Описание продукта 3'
                    ],
                ],
            ],
        ];
    }


    public function showProduct($cat_id, $prod_id)
    {
        if (isset($this->categories[$cat_id])) {
            if (isset($this->categories[$cat_id]['products'][$prod_id])) {
                return view('products.productOne', [
                    'product' => $this->categories[$cat_id]['products'][$prod_id],
                    'category' => $this->categories[$cat_id]['name'],
                    'title' => $this->categories[$cat_id]['products'][$prod_id]['name'],
                    'cat_id' => $cat_id,
                    'prod_id' => $prod_id
                ]);
            } else {
                //
            }
        } else {
            //
        }
    }

    public function showCategory($id)
    {
        if (isset($this->categories[$id])) {
            return view('products.showCategory', [
                'category' => $this->categories[$id]['name'],
                'products' => $this->categories[$id]['products'],
                'title' => $this->categories[$id]['name'],
                'id' => $id
            ]);
        } else {
            //
        }
    }


    public function showCategoryList()
    {
        $categories = [];
        foreach ($this->categories as $key => $category) {
            $categories[] = [
                'name' => $category['name'],
                'count' => count($category['products']),
                'id'=>$key
            ];
        }

        return view('products.showCategoryList', [
            'categories' => $categories,
            'title' => 'Categories list'
        ]);
    }
}
