<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseController extends Controller
{
    public function show()
    {
        return response('Hello World', 200)->withHeaders([
          'Content-Type' => 'text/plain',
          'X-Header-One' => 'Header Value',
          'X-Header-Two' => 'Header Value',
        ]);
    }


    public function show2()
    {
        return response()
          ->view('response.test')
          ->header('Content-Type', 'text/plain');
    }
}
